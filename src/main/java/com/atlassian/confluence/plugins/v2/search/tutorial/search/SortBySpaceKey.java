package com.atlassian.confluence.plugins.v2.search.tutorial.search;

import com.atlassian.confluence.search.v2.SearchFieldNames;
import com.atlassian.confluence.search.v2.SearchSort;
import com.atlassian.confluence.search.v2.sort.FieldSort;

public class SortBySpaceKey implements SearchSort {
    private static final String KEY = "sortBySpaceKey";

    private final Order order;

    public SortBySpaceKey(Order order) {
        this.order = order;
    }

    @Override
    public String getKey() {
        return KEY;
    }

    @Override
    public Order getOrder() {
        return order;
    }

    @Override
    public SearchSort expand() {
        return new FieldSort(SearchFieldNames.SPACE_KEY, Type.STRING, order);
    }
}

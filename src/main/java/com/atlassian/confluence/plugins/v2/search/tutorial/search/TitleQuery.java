package com.atlassian.confluence.plugins.v2.search.tutorial.search;

import com.atlassian.confluence.search.v2.BooleanOperator;
import com.atlassian.confluence.search.v2.SearchFieldNames;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.PrefixQuery;
import com.atlassian.confluence.search.v2.query.QueryStringQuery;

import java.util.List;

import static java.util.Collections.singletonList;

public class TitleQuery implements SearchQuery {
    private static final String KEY = "titleQuery";

    private final String query;

    public TitleQuery(String query) {
        this.query = query;
    }

    @Override
    public String getKey() {
        return KEY;
    }

    @Override
    public List getParameters() {
        return singletonList(query);
    }

    @Override
    public SearchQuery expand() {
        return BooleanQuery.builder()
                .addShould(new QueryStringQuery(singletonList(SearchFieldNames.TITLE), query, BooleanOperator.OR))
                .addShould(new PrefixQuery(SearchFieldNames.CONTENT_NAME_UNSTEMMED_FIELD, query))
                .addShould(new PrefixQuery(SearchFieldNames.PARENT_TITLE_UNSTEMMED_FIELD, query))
                .build();
    }
}

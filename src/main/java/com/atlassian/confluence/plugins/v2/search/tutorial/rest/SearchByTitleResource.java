package com.atlassian.confluence.plugins.v2.search.tutorial.rest;

import com.atlassian.confluence.plugins.v2.search.tutorial.search.SortBySpaceKey;
import com.atlassian.confluence.plugins.v2.search.tutorial.search.TitleQuery;
import com.atlassian.confluence.search.v2.ContentSearch;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.search.v2.SearchSort;
import com.atlassian.confluence.search.v2.searchfilter.SiteSearchPermissionsSearchFilter;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;

@Path("/search")
@Consumes(value = MediaType.APPLICATION_JSON)
@Produces(value = MediaType.APPLICATION_JSON)
public class SearchByTitleResource {
    private final SearchManager searchManager;

    @Autowired
    public SearchByTitleResource(@ComponentImport SearchManager searchManager) {
        this.searchManager = requireNonNull(searchManager);
    }

    @Path("/title")
    @GET
    public List<Map<String, String>> search(@QueryParam("query") String query) {
        SearchResults result;
        try {
            result = searchManager.search(new ContentSearch(
                    new TitleQuery(query),
                    new SortBySpaceKey(SearchSort.Order.ASCENDING),
                    SiteSearchPermissionsSearchFilter.getInstance(),
                    0, 10));
        } catch (InvalidSearchException e) {
            throw new RuntimeException(e);
        }

        List<Map<String, String>> response = new ArrayList<>();
        result.forEach(x ->
                response.add(ImmutableMap.of("title", x.getDisplayTitle(), "url", x.getUrlPath())));

        return response;
    }

}
